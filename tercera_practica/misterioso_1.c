#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	pid_t childpid;
	int status, exit_code, x;

	exit_code = EXIT_SUCCESS;

	if(argc < 2)
	{
		printf("Usage: %s command args\n", argv[0]);
		exit_code = EXIT_FAILURE;
	}
	else
	{
		switch (childpid = fork())
		{
			case -1:
				perror("Could not fork\n");
				exit_code = EXIT_FAILURE;
				break;
			case 0:
				if (execvp(argv[1], &argv[1]) < 0)
				{
					perror("Could not execute the command\n");
					exit_code = EXIT_FAILURE;
					break;
				}
			default: 
				if((x = wait(&status)) != childpid)
				{
					perror("Wait interrupted by a signal\n");
					exit_code = EXIT_FAILURE;
				}		
		}// end switch


	}// end else	
	
	exit(exit_code);
}// end main
