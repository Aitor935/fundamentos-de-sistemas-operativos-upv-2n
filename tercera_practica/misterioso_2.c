#include <sys/stat.h>
#include <fcntl.h>  
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define TRUE 1
#define FALSE 0
#define BLKSIZE 1
#define NEWFILE (O_WRONLY | O_CREAT | O_EXCL)
#define MODE600 (S_IRUSR | S_IWUSR) 

int main(int argc, char *argv[])
{
	int from_fd, to_fd, count, exit_code;
	int written_flag,  read_flag, end_of_file_flag;

	char buf[BLKSIZE];

	exit_code = EXIT_SUCCESS;

	if(argc != 3)
	{
		printf("Usage: %s from_file to_file\n", argv[0]);
		exit_code = EXIT_FAILURE;
	}
	else
	{
		if((from_fd = open(argv[1], O_RDONLY)) < 0)
		{
			perror("Could not open the source file\n");
			exit_code = EXIT_FAILURE;
		}
		else
		{
			if ((to_fd = open(argv[2], NEWFILE, MODE600)) < 0)
			{
				perror("Could not create the destination file\n");
				exit_code = EXIT_FAILURE;
			}
			else
			{

				written_flag = TRUE;
				read_flag = TRUE;
				end_of_file_flag = FALSE;
				while (written_flag && read_flag && !end_of_file_flag)
				{
					switch (count = read(from_fd, buf, sizeof(buf)))
					{
						case -1:
							read_flag = FALSE;
							perror("Could not read from source\n");
							exit_code = EXIT_FAILURE;
							break;
						case 0:
							end_of_file_flag = TRUE;
							break;
						default:
							if (write(to_fd, buf, count) != count)
							{
								written_flag = FALSE;
								perror("Could not write to destination\n");
								exit_code = EXIT_FAILURE;
							}
						} //switch
					} // while
					if (close(to_fd) != 0)
					{
						perror("Could not close the destination file\n");
						exit_code = EXIT_FAILURE;
					}
				} // else open to_fd
				if (close(from_fd) != 0)
				{
					perror("Could not close the source file\n");
					exit_code = EXIT_FAILURE;
				}
			} // else open from_fd
		} // else argc
	exit(exit_code);
} // main


