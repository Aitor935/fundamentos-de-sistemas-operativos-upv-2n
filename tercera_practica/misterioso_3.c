#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define NEWFILE (O_WRONLY | O_CREAT | O_EXCL)
#define MODE644 (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

int redirect_output(const char *file)
{
	int return_code, fd;
	
	return_code = 0;

	if ((fd = open(file, NEWFILE, MODE644)) < 0)
	{
		return_code = -1;
	}
	else
	{
		if (dup2(fd, STDOUT_FILENO) < 0)
		{
			return_code = -1;
		}
		else
		{
			if (close(fd) != 0)
			{
				return_code = -1;
			}
		}
	}
	return return_code;
}


int main(int argc, char *argv[])
{
	int exit_code;

	exit_code = EXIT_SUCCESS;

	if (argc < 3)
	{
		printf("Usage: %s to_file command args\n", argv[0]);
		exit_code = EXIT_FAILURE;
	}
	else
	{
		if (redirect_output(argv[1]) == -1)
		{
			perror("Could not redirect the output\n");
			exit_code = EXIT_FAILURE;
		}
		else
		{
			if (execvp(argv[2], &argv[2]) < 0)
			{
				perror("Could not execute the command\n");
				exit_code = EXIT_FAILURE;
			}
		}
	}
	exit(exit_code);
}


